import 'package:flutter/material.dart';
import 'package:jo_gym_app/config/config_color.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';

class ComponentJogymMainButton extends StatelessWidget {
  const ComponentJogymMainButton({
    super.key,
    required this.buttonText,
    required this.voidCallback,
  });

  final String buttonText; // 제목
  final VoidCallback voidCallback; // 야 클릭됐다~~ 니가 처리해~~ 신호주기

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: MediaQuery
            .of(context)
            .size
            .width * 0.44,
        child: ElevatedButton(
          onPressed: voidCallback,
          style: ElevatedButton.styleFrom(
              foregroundColor: Colors.black,
              backgroundColor: colorLightGray,
              padding: contentPaddingButton,
              elevation: buttonElevation,
              textStyle: const TextStyle(
                fontSize: fontSizeMid,
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                      buttonRadius),
                  side: const BorderSide(
                    color: colorLightGray,
                  )
              )
          ),
          child: Padding(
            padding: const EdgeInsets.only(top: 15, bottom: 15),
            child: Text(
              buttonText,
              style: const TextStyle(
                  fontSize: fontSizeSuper
              ),
            ),
          ),
        ),
    );
  }
}