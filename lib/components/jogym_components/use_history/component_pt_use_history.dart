import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_size.dart';

class ComponentPtUseHistory extends StatelessWidget {
  const ComponentPtUseHistory(
      {super.key,
        required this.number,
        required this.dateUse,
        required this.name,
        required this.trainerName,
        required this.maxCount,
        required this.useCount,
        required this.remainCount,
        required this.voidCallback,});

  final int number;
  final String dateUse;
  final String name;
  final String trainerName;
  final int maxCount;
  final int useCount;
  final int remainCount;
  final VoidCallback voidCallback;

  // No
  // 사용일시
  // PT권명
  // 트레이너 이름
  // 횟수
  // 사용횟수
  // 잔여횟수

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 10),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black, width: 2),
        ),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      // No
                      // 사용일시
                      // PT권명
                      // 트레이너 이름
                      // 횟수
                      // 사용횟수
                      // 잔여횟수
                      _setRowText(subTitle: "No.", subTitleVal: "$number"),
                      _setRowText(subTitle: "사용일시", subTitleVal: dateUse),
                      _setRowText(subTitle: "PT권명", subTitleVal: name),
                      _setRowText(subTitle: "트레이너", subTitleVal: trainerName),
                      _setRowText(subTitle: "횟수", subTitleVal: "$maxCount"),
                      _setRowText(subTitle: "사용횟수", subTitleVal: "$useCount"),
                      _setRowText(subTitle: "잔여횟수", subTitleVal: "$remainCount"),
                      SizedBox(height: 10)
                    ],
                  ),
                ),
                //Container(width: 100, height: 100, child: profileImg),
              ],
            ),
          ],
        ));
  }

  Widget _setRowText({required String subTitle, required String subTitleVal}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: 100,
          child: Text(
            subTitle,
            style: const TextStyle(
              fontSize: fontSizeMid,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        Container(
          child: Text(
            subTitleVal,
            style: const TextStyle(
              fontSize: fontSizeMid,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
      ],
    );
  }
}
