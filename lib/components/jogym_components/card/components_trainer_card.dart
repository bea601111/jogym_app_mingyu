import 'package:flutter/material.dart';
import 'package:jo_gym_app/config/config_size.dart';

class ComponentTrainerCard extends StatelessWidget {
  const ComponentTrainerCard({super.key,
    required this.number,
    required this.dateCreate,
    required this.trainerName,
    required this.phoneNumber,
    required this.profileImg,
    required this.voidCallback});

  final int number;
  final String dateCreate;
  final String trainerName;
  final String phoneNumber;
  final Image profileImg;
  final VoidCallback voidCallback; // 야 클릭됐다~~ 니가 처리해~~ 신호주기

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: voidCallback,
      child: Container(
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black, width: 2),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 300,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          child: Text(
                            "No",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          width: 80,
                        ),
                        Container(
                          child: Text(
                            "$number",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 80,
                          child: Text(
                            "등록일",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),

                        ),
                        Container(
                          child: Text(
                            dateCreate,
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          child: Text(
                            "직원명",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          width: 80,
                        ),
                        Container(
                          child: Text(
                            trainerName,
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          child: Text(
                            "연락처",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          width: 80,
                        ),
                        Container(
                          child: Text(
                            phoneNumber,
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 100,
                height: 100,
                child: profileImg,
              ),
            ],
          )),
    );
  }
}

