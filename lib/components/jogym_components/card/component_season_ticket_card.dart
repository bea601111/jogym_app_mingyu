import 'package:flutter/material.dart';
import 'package:jo_gym_app/config/config_size.dart';

class ComponentSeasonTicketCard extends StatelessWidget {
  const ComponentSeasonTicketCard({super.key,
    required this.id, // 얘네는 필수
    required this.maxMonth,
    required this.ticketName,
    required this.ticketType,
    required this.totalPrice,
    required this.unitPrice,
    this.profileImg,
    required this.voidCallback // 얘네는 필수
  });

  final int id; // No
  final int maxMonth; // 최대 월
  final String ticketName; // 정기권 명
  final String ticketType; // 정기권 타입
  final num totalPrice; // 총 금액
  final num unitPrice; // 단위 금액
  final Image? profileImg;
  final VoidCallback voidCallback; // 야 클릭됐다~~ 니가 처리해~~ 신호주기

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: voidCallback,
      child: Container(
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black, width: 2),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: 300,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const SizedBox(
                          width: 80,
                          child: Text(
                            "No",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          // margin: EdgeInsets.only(right: 10),
                        ),
                        Text(
                          "$id",
                          style: const TextStyle(
                            fontSize: fontSizeMid,
                            fontWeight: FontWeight.w400,
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const SizedBox(
                          width: 80,
                          child: Text(
                            "정기권 명",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          // margin: EdgeInsets.only(right: 10),
                        ),
                        Text(
                          ticketName,
                          style: const TextStyle(
                            fontSize: fontSizeMid,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const SizedBox(
                          width: 80,
                          child: Text(
                            "타입",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            ticketType,
                            style: const TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const SizedBox(
                          width: 80,
                          child: Text(
                            "최대 월",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Text(
                          "$maxMonth",
                          style: const TextStyle(
                            fontSize: fontSizeMid,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const SizedBox(
                          width: 80,
                          child: Text(
                            "총 금액",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Text(
                          "$totalPrice",
                          style: const TextStyle(
                            fontSize: fontSizeMid,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const SizedBox(
                          width: 80,
                          child: Text(
                            "단위 금액",
                            style: TextStyle(
                              fontSize: fontSizeMid,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Text(
                          "$unitPrice",
                          style: const TextStyle(
                            fontSize: fontSizeMid,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 100,
                height: 100,
                child: profileImg,
              ),
            ],
          )),
    );
  }
}

