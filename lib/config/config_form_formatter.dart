import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

var maskLicenseInputFormatter = MaskTextInputFormatter(
    mask: 'S#-##-######-##',
    filter: {"#": RegExp(r'[0-9]'), "S": RegExp(r'[1-2]')},
    type: MaskAutoCompletionType.lazy);

var maskPhoneNumberInputFormatter = MaskTextInputFormatter(
    mask: '###-####-####',
    filter: {"#": RegExp(r'[0-9]')},
    type: MaskAutoCompletionType.lazy);

var maskAuthNumberInputFormatter = MaskTextInputFormatter(
    mask: '######',
    filter: {"#": RegExp(r'[0-9]')},
    type: MaskAutoCompletionType.lazy);

var maskMonthInputFormatter = MaskTextInputFormatter(
    mask: '##',
    filter: { "#": RegExp(r'[0-9]')},
    type: MaskAutoCompletionType.lazy
);





// var maskWonInputFormatter = MaskTextInputFormatter(
//     mask: '###,###,###',
//     filter: { "#": RegExp(r'[0-9]')},
//     type: MaskAutoCompletionType.lazy
// );

class MaskWonInputFormatter extends TextInputFormatter {
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {

      if(newValue.selection.baseOffset == 0){
        print(true);
        return newValue;
      }
      String str = newValue.text.replaceAll(RegExp(r'\₩|\,'), "");

      double value = double.parse(str);
      final formatter = NumberFormat('###,###,###,###');
      String newText = formatter.format(value);

      return newValue.copyWith(
          text: newText,
          selection: TextSelection.collapsed(offset: newText.length));
    }
  }



var maskWonFormatter = NumberFormat.currency(locale: "ko_KR", symbol: "");
var maskNumFormatter = NumberFormat('###,###,###,###');
var maskNoInputKrFormatter =
    FilteringTextInputFormatter.allow(RegExp('[0-9a-zA-Z]'));
