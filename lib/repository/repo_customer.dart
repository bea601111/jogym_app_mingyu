import 'package:dio/dio.dart';
import 'package:jo_gym_app/config/config_api.dart';
import 'package:jo_gym_app/config/config_api_customer.dart';
import 'package:jo_gym_app/functions/token_lib.dart';
import 'package:jo_gym_app/model/common_result.dart';
import 'package:jo_gym_app/model/customer_item_result.dart';
import 'package:jo_gym_app/model/license_update_request.dart';
import 'package:jo_gym_app/model/login_request.dart';
import 'package:jo_gym_app/model/login_result.dart';
import 'package:jo_gym_app/model/customer_create_request.dart';
import 'package:jo_gym_app/model/member_join_request.dart';
import 'package:jo_gym_app/model/member_password_find_request.dart';
import 'package:jo_gym_app/model/my_profile_status_item.dart';
import 'package:jo_gym_app/model/my_profile_status_item_result.dart';
import 'package:jo_gym_app/model/password_change_request.dart';

class RepoCustomer {
  Future<CommonResult> doCreate(CustomerCreateRequest request) async {

    const String baseUrl = '$apiUriCustomer/customer/data';
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.post(baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CommonResult.fromJson(response.data);
  }

  Future<CustomerItemResult> getList({int page = 1}) async {
    const String baseUrl = '$apiUriCustomer/customer/list?page={page}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(baseUrl.replaceAll('{page}', page.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));
    return CustomerItemResult.fromJson(response.data);
  }
}
