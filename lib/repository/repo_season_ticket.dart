import 'package:dio/dio.dart';
import 'package:jo_gym_app/config/config_api.dart';
import 'package:jo_gym_app/config/config_api_goods.dart';
import 'package:jo_gym_app/config/config_api_season_ticket.dart';
import 'package:jo_gym_app/functions/token_lib.dart';
import 'package:jo_gym_app/model/common_result.dart';
import 'package:jo_gym_app/model/season_ticket_create_request.dart';
import 'package:jo_gym_app/model/season_ticket_item_result.dart';

class RepoSeasonTicket {

  Future<CommonResult> setSeasonTicket(SeasonTicketCreateRequest request) async {
    const String baseUrl = '$apiUriSeasonTicket/season-ticket/data';

    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';
    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<SeasonTicketItemResult> getList({int page = 1}) async {
    const String baseUrl = '$apiUriGoods/season-ticket/list?page={page}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(baseUrl.replaceAll('{page}', page.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));
    return SeasonTicketItemResult.fromJson(response.data);
  }
  Future<CommonResult> doSeasonTicketChange(int id, SeasonTicketCreateRequest request) async {
    const String baseUrl = '$apiUriSeasonTicket/season-ticket/{id}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(baseUrl.replaceAll('{id}', id.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }
  Future<CommonResult> doSeasonTicketDelete(int id) async {
    const String baseUrl = '$apiUriSeasonTicket/season-ticket/ticket-id/{id}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }
}