import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_custom_loading.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_notification.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_form_validator.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/model/pt_ticket_create_request.dart';
import 'package:jo_gym_app/pages/page_home.dart';
import 'package:jo_gym_app/pages/page_login.dart';
import 'package:jo_gym_app/pages/pt/page_pt_ticket_create_trainer_list.dart';
import 'package:jo_gym_app/repository/repo_pt_ticket.dart';
import 'package:jo_gym_app/styles/style_form_decoration.dart';

class PagePtTicketCreate extends StatefulWidget {
  const PagePtTicketCreate({super.key});

  @override
  State<PagePtTicketCreate> createState() => _PagePtTicketCreateState();
}

class TrainerData {
  TrainerData({required this.id, required this.name, required this.isSave});

  int id;
  String name;
  bool isSave;
}

class _PagePtTicketCreateState extends State<PagePtTicketCreate> {
  final _formKey = GlobalKey<FormBuilderState>();
  TrainerData trainerData = TrainerData(id: 0, name: "", isSave: false);

  Future<void> _setPtTicket(PtTicketCreateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoPtTicket().setPtTicket(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: 'PT 등록 완료',
        subTitle: 'PT 등록이 완료되었습니다.',
      ).call();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageHome()),
          (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: 'PT 등록 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  void initState() {
    print('initData');
    print('id : ${trainerData.id}');
    print("name : ${trainerData.name}");
    print('isEnable : ${trainerData.isSave}');
    super.initState();
  }

  // "trainerId"
  // "maxMonth": 0,
  // "ticketName": "string",
  // "unitPrice": 0

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: 'Pt 정보 등록'),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Container(
                padding: bodyPaddingLeftRight,
                child: FormBuilder(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.disabled, // 자동 유효성 검사 비활성화
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'ticketName',
                          decoration:
                              StyleFormDecoration().getInputDecoration('PT권명'),
                          maxLength: 20,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            FormBuilderValidators.minLength(2,
                                errorText: formErrorMinLength(2)),
                            FormBuilderValidators.maxLength(20,
                                errorText: formErrorMaxLength(20)),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(),
                      GestureDetector(
                        child: Container(
                          decoration: formBoxDecoration,
                          padding: bodyPaddingAll,
                          child: FormBuilderTextField(
                            name: 'totalSet',
                            decoration: StyleFormDecoration()
                                .getInputDecoration('총 횟수'),
                            maxLength: 20,
                            keyboardType: TextInputType.text,
                            validator: FormBuilderValidators.compose([
                              FormBuilderValidators.required(
                                  errorText: formErrorRequired),
                              FormBuilderValidators.minLength(1,
                                  errorText: formErrorMinLength(1)),
                              FormBuilderValidators.maxLength(20,
                                  errorText: formErrorMaxLength(20)),
                            ]),
                          ),
                        ),
                        onTap: () async {
                          final selectData = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      PagePtTicketCreateTrainerList()));

                          setState(() {
                            if (selectData != null) {
                              trainerData = selectData;
                              print('SelectData');
                              print('id : ${selectData.id}');
                              print("name : ${selectData.name}");
                              print('isEnable : ${selectData.isSave}');
                            }
                          });
                        },
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'totalSet',
                          decoration:
                              StyleFormDecoration().getInputDecoration('총 횟수'),
                          maxLength: 20,
                          keyboardType: TextInputType.text,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            FormBuilderValidators.minLength(1,
                                errorText: formErrorMinLength(1)),
                            FormBuilderValidators.maxLength(20,
                                errorText: formErrorMaxLength(20)),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'setPrice',
                          decoration: StyleFormDecoration()
                              .getInputDecoration('횟수 / 요금'),
                          maxLength: 20,
                          keyboardType: TextInputType.text,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            FormBuilderValidators.minLength(5,
                                errorText: formErrorMinLength(5)),
                            FormBuilderValidators.maxLength(20,
                                errorText: formErrorMaxLength(20)),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(enumSize: EnumSize.mid),
                      Container(
                        padding: bodyPaddingLeftRight,
                        child: ComponentTextBtn(
                          '수정 / 저장',
                          () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) => PageLogin()),
                          ),
                          bgColor: Color.fromRGBO(84, 89, 87, 100),
                          borderColor: Colors.white,
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        padding: bodyPaddingLeftRight,
                        child: ComponentTextBtn(
                          '삭제',
                          () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) => PageLogin()),
                          ),
                          bgColor: Color.fromRGBO(84, 89, 87, 100),
                          borderColor: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
