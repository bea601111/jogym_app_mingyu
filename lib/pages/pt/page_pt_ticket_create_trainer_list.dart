import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_count_title.dart';
import 'package:jo_gym_app/components/common/component_custom_loading.dart';
import 'package:jo_gym_app/components/common/component_no_contents.dart';
import 'package:jo_gym_app/pages/pt/page_pt_ticket_create.dart';

class PagePtTicketCreateTrainerList extends StatefulWidget {
  const PagePtTicketCreateTrainerList({super.key});

  @override
  State<PagePtTicketCreateTrainerList> createState() =>
      _PagePtTicketCreateTrainerListState();
}

class _PagePtTicketCreateTrainerListState
    extends State<PagePtTicketCreateTrainerList> {

  final _scrollController = ScrollController();

  List<String> _list = [];
  int _totalItemCount = 0;
  int _totalPage = 1;
  int _currentPage = 1;


  Future<void> _loadItems({bool reFresh = false}) async {
    // 새로고침하면 초기화시키기.
    if (reFresh) {
      _list = [];
      _totalItemCount = 0;
      _totalPage = 1;
      _currentPage = 1;
    }

    if (_currentPage <= _totalPage) {
      // BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      //   return ComponentCustomLoading(
      //     cancelFunc: cancelFunc,
      //   );
      // });

      // await RepoKickBoard()
      //     .getList(page: _currentPage)
      //     .then((res) =>
      // {
      //   BotToast.closeAllLoading(),
      //   setState(() {
      //     _totalItemCount = res.totalItemCount;
      //     _totalPage = res.totalPage;
      //     _list = [..._list, ...?res.list]; // 기존 리스트에 api에서 받아온 리스트 데이터를 더하는거.
      //     _currentPage++;
      //   })
      // })
      //     .catchError((err) =>
      // {
      //   BotToast.closeAllLoading(),
      //   print(err),
      // });
    }

    // 새로고침하면 스크롤위치를 맨 위로 올리기.
    if (reFresh) {
      _scrollController.animateTo(
        0,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });
    //_loadItems();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: '트레이너 정보'),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _loadItems(reFresh: true);
          Navigator.pop(context, TrainerData(id: 1, name: "로니로니", isSave: true));
        },
        child: const Icon(Icons.refresh),
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          _buildBody(),
        ],
      ),
    );
  }
  Widget _buildBody() {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ComponentCountTitle(Icons.attachment, _totalItemCount, '건', '킥보드 리스트'),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) {

            }
          )
        ],
      );
    } else {
      // 이용내역이 없을때 흰화면 보이면 에러난것처럼 보이니까 이용내역 없다고 보여주기.
      return SizedBox(
        height: MediaQuery
            .of(context)
            .size
            .height - 30 - 50 - 50 - 70,
        child: GestureDetector(
          child: const ComponentNoContents(
            icon: Icons.history,
            msg: '등록된 트레이너가 없습니다.',
          ),
          onTap: () => _loadItems(reFresh: true),
        ),
      );
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}

