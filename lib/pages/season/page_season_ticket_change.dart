import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_custom_loading.dart';
import 'package:jo_gym_app/components/common/component_margin_horizon.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_notification.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/config/config_color.dart';
import 'package:jo_gym_app/config/config_form_formatter.dart';
import 'package:jo_gym_app/config/config_form_validator.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/model/season_ticket_create_request.dart';
import 'package:jo_gym_app/pages/season/page_season_ticket.dart';
import 'package:jo_gym_app/repository/repo_season_ticket.dart';
import 'package:jo_gym_app/styles/style_form_decoration.dart';

class PageSeasonTicketChange extends StatefulWidget {
  const PageSeasonTicketChange({super.key, required this.ticketName, required this.maxMonth, required this.unitPrice, required this.id});

  final int id;
  final String ticketName;
  final int maxMonth;
  final double unitPrice;

  @override
  State<PageSeasonTicketChange> createState() => _PageSeasonTicketChangeState();
}

// "ticketName": "string",
// "ticketType": "MONTH",
// "maxMonth": 0,
// "unitPrice": 0

class _PageSeasonTicketChangeState extends State<PageSeasonTicketChange> {
  final _formKey = GlobalKey<FormBuilderState>();

  final TextEditingController _textControllerOfTicketName =
      TextEditingController();
  final TextEditingController _textControllerOfUnitPrice =
      TextEditingController();
  bool _isFixEnable = false;
  int? option;

  _ShowDialog({
    required String titleText,
    required Color titleColor,
    required String contentText,
    required Color contentColor,
    required VoidCallback callbackOk,
    required VoidCallback callbackCancel,
  })
  {
    showDialog(
        context: context,
        //barrierDismissible - Dialog를 제외한 다른 화면 터치 x
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            // RoundedRectangleBorder - Dialog 화면 모서리 둥글게 조절
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            //Dialog Main Title
            title: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(Icons.report_problem_outlined, color: titleColor),
                ComponentMarginHorizon(enumSize: EnumSize.mid),
                Text(titleText,
                    style: TextStyle(
                        color: titleColor,
                        fontSize: fontSizeBig,
                        fontWeight: FontWeight.bold)),
              ],
            ),
            content: Text(contentText,
                style: TextStyle(
                    color: contentColor,
                    fontSize: fontSizeMid,
                    fontWeight: FontWeight.bold)),
            actions: [
              TextButton(
                child: Text("확인"),
                onPressed: callbackOk,
              ),
              TextButton(
                child: Text("취소"),
                onPressed: callbackCancel,
              ),
            ],
          );
        });
  }

  @override
  void initState() {
    super.initState();
    _textControllerOfTicketName.text = widget.ticketName;
    _textControllerOfUnitPrice.text = maskNumFormatter.format(widget.unitPrice);
  }
  Future<void> _putSeasonTicket(SeasonTicketCreateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoSeasonTicket()
        .doSeasonTicketChange(widget.id, request)
        .then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '정기권 수정 완료',
        subTitle: '정기권 수정 완료 되었습니다.',
      ).call();
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '정기권 수정 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }
  Future<void> _putSeasonTicketDelete() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoSeasonTicket()
        .doSeasonTicketDelete(widget.id)
        .then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '정기권 삭제 완료',
        subTitle: '정기권 삭제 완료 되었습니다.',
      ).call();
      Navigator.pop(context);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '정기권 삭제 실패',
        subTitle: '문의처에 연락해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "정기권 정보 수정"),
      body: Container(
        child: SingleChildScrollView(
          padding: bodyPaddingLeftRight,
          child: Column(
            children: [
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Container(
                child: FormBuilder(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.disabled, // 자동 유효성 검사 비활성화
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'ticketName',
                          controller: _textControllerOfTicketName,
                          enabled: _isFixEnable,
                          decoration:
                              StyleFormDecoration().getInputDecoration('정기권명'),
                          maxLength: 20,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                            FormBuilderValidators.minLength(2,
                                errorText: formErrorMinLength(2)),
                            FormBuilderValidators.maxLength(20,
                                errorText: formErrorMaxLength(20)),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderDropdown<int>(
                          name: 'dailyMonth',
                          enabled: _isFixEnable,
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                          ]),
                          decoration: const InputDecoration(
                            labelText: '일일권 / 월',
                            filled: true,
                            isDense: false,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(0)),
                            ),
                          ),
                          onChanged: (value) {
                            setState(() {
                              option = value;
                            });
                          },
                          items: const [
                            DropdownMenuItem(value: 0, child: Text('일일권')),
                            DropdownMenuItem(value: 1, child: Text('정기권')),
                          ],
                        ),
                      ),
                      Visibility(
                        visible: option == 1,
                        maintainState: option == 1,
                        child: Container(
                          decoration: formBoxDecoration,
                          padding: bodyPaddingAll,
                          child: FormBuilderTextField(
                            name: 'maxMonth',
                            decoration:
                            StyleFormDecoration().getInputDecoration('개월'),
                            keyboardType: TextInputType.text,
                            inputFormatters: [maskMonthInputFormatter],
                            validator: FormBuilderValidators.compose([
                              FormBuilderValidators.required(
                                  errorText: formErrorRequired),
                              FormBuilderValidators.min(1,
                                  errorText: formErrorMinNumber(1)),
                            ]),
                          ),
                        ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        decoration: formBoxDecoration,
                        padding: bodyPaddingAll,
                        child: FormBuilderTextField(
                          name: 'unitPrice',
                          controller: _textControllerOfUnitPrice,
                          enabled: _isFixEnable,
                          decoration:
                              StyleFormDecoration().getInputDecoration('월 요금'),
                          keyboardType: TextInputType.number,
                          inputFormatters: [MaskWonInputFormatter()],
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(
                                errorText: formErrorRequired),
                          ]),
                        ),
                      ),
                      const ComponentMarginVertical(enumSize: EnumSize.mid),
                      Container(
                        padding: bodyPaddingLeftRight,
                        child: !_isFixEnable
                            ? ComponentTextBtn(
                                '수정',
                                () {
                                  setState(() {
                                    _isFixEnable = !_isFixEnable;
                                  });
                                },
                                bgColor: Color.fromRGBO(49, 176, 121, 100),
                                borderColor: Colors.white,
                              )
                            : ComponentTextBtn(
                                '저장',
                                () {
                                  String tempUnit = _formKey.currentState!.fields['unitPrice']!.value;
                                  SeasonTicketCreateRequest request;
                                  if (_formKey.currentState!.saveAndValidate()) {
                                    if(option == 1){
                                      request = SeasonTicketCreateRequest(
                                          _formKey.currentState!.fields['ticketName']!.value,
                                          int.parse(_formKey.currentState!.fields['maxMonth']!.value),
                                          double.parse(tempUnit.replaceAll(',', ""))
                                      );
                                      _putSeasonTicket(request);
                                      Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(builder: (BuildContext context) => const PageSeasonTicket()),
                                              (route) => false
                                      );
                                    } else {
                                      request = SeasonTicketCreateRequest(
                                          _formKey.currentState!.fields['ticketName']!.value,
                                          0,
                                          double.parse(tempUnit.replaceAll(',', ""))
                                      );
                                      _putSeasonTicket(request);
                                      Navigator.pop(context);
                                    }
                                    // print(
                                    //     "ticketName : ${_formKey.currentState!.fields['ticketName']!.value}");
                                    // print(
                                    //     "ticketType : ${_formKey.currentState!.fields['ticketType']!.value}");
                                    // print(
                                    //     "maxMonth : ${_formKey.currentState!.fields['maxMonth']!.value}");
                                    // print(
                                    //     "unitPrice : ${_formKey.currentState!.fields['unitPrice']!.value}");
                                    //_setCustomer(request);
                                    setState(() {
                                      _isFixEnable = !_isFixEnable;
                                    });
                                  }
                                },
                                bgColor: Color.fromRGBO(49, 176, 121, 100),
                                borderColor: Colors.white,
                              ),
                      ),
                      const ComponentMarginVertical(),
                      Container(
                        padding: bodyPaddingLeftRight,
                        child: ComponentTextBtn(
                          '삭제',
                          () {
                            _ShowDialog(
                                titleText: "정기권 삭제",
                                titleColor: colorRed,
                                contentText: "정말 삭제 하시겠습니까?",
                                contentColor: colorDarkGray,
                                callbackOk: (){
                                  _putSeasonTicketDelete();
                                  Navigator.pop(context);
                                  },
                                callbackCancel: (){Navigator.pop(context);}
                            );
                          },
                          bgColor: Color.fromRGBO(84, 89, 87, 100),
                          borderColor: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
