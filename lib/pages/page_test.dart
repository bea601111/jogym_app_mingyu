import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';

class PageTest extends StatefulWidget {
  const PageTest({super.key});

  @override
  State<PageTest> createState() => _PageTestState();
}

List<String> values = [
  '1',
  'one',
  'two',
  'three',
  'a',
  'one',
  'two',
  'three',
  'a',
  'one',
  'two',
  'three',
  'a'
];

class _PageTestState extends State<PageTest> {
  final _scrollController = ScrollController();
  int _totalItemCount = 0;
  int _totalPage = 1;
  int _currentPage = 1;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        setState(() {
          for (int i = 0; i < 10; i++) {
            values.add("$i");
          }
        });

        print("페이징");
      }
    });
    //_loadItems();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: 'Pt 정보 등록'),
      body: _showDialog(),
    );
  }

  Widget _showDialog() {
    return SimpleDialog(
      title: Text("Select Value"),
      children: [
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height / 4,
          child: ListView.builder(
            controller: _scrollController,
            //physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: values.length,
            itemBuilder: (ctx, index) {
              return SimpleDialogOption(
                onPressed: () => Navigator.pop(context),
                child: Center(
                  child: Text(values[index]),
                ),
              );
            },
          ),
        )
      ],
    );
  }
}
