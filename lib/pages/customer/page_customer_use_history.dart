import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/components/jogym_components/buy_history/component_pt_buy_history.dart';
import 'package:jo_gym_app/components/jogym_components/use_history/component_pt_use_history.dart';
import 'package:jo_gym_app/components/jogym_components/use_history/component_season_use_history.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';

class PageCustomerUseHistory extends StatefulWidget {
  const PageCustomerUseHistory({super.key});

  @override
  State<PageCustomerUseHistory> createState() => _PageCustomerUseHistoryState();
}

class _PageCustomerUseHistoryState extends State<PageCustomerUseHistory> {
  bool _isBtnSwitch = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "이용내역"),
      body: Column(
        children: [
          const ComponentMarginVertical(enumSize: EnumSize.mid),
          Container(
            padding: bodyPaddingLeftRightAndVerticalHalf,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: MediaQuery.sizeOf(context).width / 3,
                  child: (_isBtnSwitch)
                      ? ComponentTextBtn(
                          '정기권',
                          bgColor: Color.fromRGBO(49, 176, 121, 100),
                          borderColor: Colors.white,
                          () {},
                        )
                      : ComponentTextBtn('정기권',
                          bgColor: Colors.grey,
                          borderColor: Colors.white,
                          foregroundColor: Colors.white, () {
                          setState(() {
                            _isBtnSwitch = !_isBtnSwitch;
                          });
                        }),
                ),
                Container(
                  width: MediaQuery.sizeOf(context).width / 3,
                  child: (!_isBtnSwitch)
                      ? ComponentTextBtn(
                          'PT권',
                          bgColor: Color.fromRGBO(49, 176, 121, 100),
                          borderColor: Colors.white,
                          () {},
                        )
                      : ComponentTextBtn('PT권',
                          bgColor: Colors.grey,
                          borderColor: Colors.white,
                          foregroundColor: Colors.white, () {
                          setState(() {
                            _isBtnSwitch = !_isBtnSwitch;
                          });
                        }),
                ),
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              padding: bodyPaddingLeftRight,
              child: _isBtnSwitch
                  ? _setMockUpData_SeasonTicket()
                  : _setMockUpData_PtTicketHistory(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _setMockUpData_SeasonTicket() {
    return Column(
      children: [
        ComponentSeasonUseHistory(
          number: 1,
          dateUse: "2021-01-01 10:00",
          name: "일일권",
          voidCallback: () {},
        )
      ],
    );
  }

  Widget _setMockUpData_PtTicketHistory() {
    return Column(
      children: [
        ComponentPtUseHistory(
            number: 1,
            dateUse: "2020-01-01 10:20",
            name: "로니콜먼 PT",
            trainerName: "로니콜먼",
            maxCount: 20,
            useCount: 10,
            remainCount: 10,
            voidCallback: () {

            },)
      ],
    );
  }
}
