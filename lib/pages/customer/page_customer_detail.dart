import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/common/component_margin_vertical.dart';
import 'package:jo_gym_app/components/common/component_text_btn.dart';
import 'package:jo_gym_app/components/component_divider.dart';
import 'package:jo_gym_app/config/config_form_formatter.dart';
import 'package:jo_gym_app/config/config_form_validator.dart';
import 'package:jo_gym_app/config/config_size.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/enums/enum_size.dart';
import 'package:jo_gym_app/pages/customer/page_customer_buy_history.dart';
import 'package:jo_gym_app/pages/customer/page_customer_buy_ticket.dart';
import 'package:jo_gym_app/pages/customer/page_customer_use_history.dart';
import 'package:jo_gym_app/styles/style_form_decoration_full_going.dart';

class PageCustomerDetail extends StatefulWidget {
  const PageCustomerDetail({super.key, required this.customerId});

  final int customerId;

  @override
  State<PageCustomerDetail> createState() => _PageCustomerDetailState();
}

class _PageCustomerDetailState extends State<PageCustomerDetail> {
  final _formKey = GlobalKey<FormBuilderState>();
  final TextEditingController _textControllerOfName = TextEditingController();
  final TextEditingController _textControllerOfPhoneNumber =
      TextEditingController();
  final TextEditingController _textControllerOfAddress =
      TextEditingController();
  final TextEditingController _textControllerOfMemo = TextEditingController();

  String _strDateCreate = "2023-04-12";
  String _initValueOfGender = "MALE";
  DateTime _initDateBirth = DateTime(2023, 01, 01);
  bool _isFixEnable = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      _textControllerOfName.text = "홍길동";
      _textControllerOfPhoneNumber.text = "010-1234-1234";
      _textControllerOfAddress.text = "경기도 안산시 단원구 고잔동 539-2";
      _initValueOfGender = "FEMALE";
      _initDateBirth = DateTime(2023, 01, 01);
      _textControllerOfMemo.text =
          "수상내역 이런저런\n경력 너무많음\n자격증 이것저것\n하지정맥 수술 경험\n매일 늦음";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: '회원 상세 정보 관리'),
      body: SingleChildScrollView(
        padding: bodyPaddingLeftRight,
        child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const ComponentMarginVertical(enumSize: EnumSize.mid),
                Container(
                  alignment: Alignment.centerLeft,
                  width: 200,
                  height: 200,
                  child: Image.asset("assets/ulgen.png"),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.mid),
                Text("등록일 : $_strDateCreate",
                    style: TextStyle(
                        fontSize: fontSizeBig, fontWeight: FontWeight.w500)),
                const ComponentMarginVertical(),
                ComponentDivider(),
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'name',
                  controller: _textControllerOfName,
                  enabled: _isFixEnable,
                  decoration: StyleFormDecorationOfInputText()
                      .getInputDecoration('회원명'),
                  maxLength: 20,
                  keyboardType: TextInputType.text,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(3,
                        errorText: formErrorMinLength(3)),
                    FormBuilderValidators.maxLength(20,
                        errorText: formErrorMaxLength(20)),
                  ]),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.micro),
                FormBuilderTextField(
                  name: 'phoneNumber',
                  controller: _textControllerOfPhoneNumber,
                  enabled: _isFixEnable,
                  decoration: StyleFormDecorationOfInputText()
                      .getInputDecoration('연락처'),
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    maskPhoneNumberInputFormatter
                    //13자리만 입력받도록 하이픈 2개+숫자 11개
                  ],
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(13,
                        errorText: formErrorMinLength(13)),
                    FormBuilderValidators.maxLength(13,
                        errorText: formErrorMaxLength(13)),
                  ]),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.micro),
                FormBuilderTextField(
                  name: 'address',
                  controller: _textControllerOfAddress,
                  enabled: _isFixEnable,
                  decoration:
                      StyleFormDecorationOfInputText().getInputDecoration('주소'),
                  keyboardType: TextInputType.text,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(3,
                        errorText: formErrorMinLength(3)),
                    FormBuilderValidators.maxLength(100,
                        errorText: formErrorMaxLength(100)),
                  ]),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.micro),
                FormBuilderDropdown<String>(
                  name: 'gender',
                  enabled: _isFixEnable,
                  initialValue: _initValueOfGender,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                  ]),
                  decoration: const InputDecoration(
                    labelText: '성별',
                    filled: true,
                    isDense: false,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                    ),
                  ),
                  items: const [
                    DropdownMenuItem(value: "MALE", child: Text('남성')),
                    DropdownMenuItem(value: "FEMALE", child: Text('여성')),
                  ],
                ),
                const ComponentMarginVertical(enumSize: EnumSize.small),
                FormBuilderDateTimePicker(
                  name: 'dateBirth',
                  initialEntryMode: DatePickerEntryMode.calendarOnly,
                  initialValue: _initDateBirth,
                  inputType: InputType.date,
                  firstDate: DateTime(1970),
                  lastDate: DateTime(2030),
                  format: DateFormat('yyyy-MM-dd'),
                  enabled: _isFixEnable,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                  ]),
                  decoration: const InputDecoration(
                    labelText: '생년월일',
                    filled: true,
                    isDense: false,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                    ),
                  ),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.small),
                FormBuilderTextField(
                  name: 'memo',
                  controller: _textControllerOfMemo,
                  enabled: _isFixEnable,
                  keyboardType: TextInputType.multiline,
                  maxLines: 4,
                  decoration:
                      StyleFormDecorationOfInputText().getInputDecoration('비고'),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.mid),
                Container(
                  width: MediaQuery.sizeOf(context).width,
                  child: ComponentTextBtn(
                    _isFixEnable ? "저장" : '수정',
                    bgColor: Color.fromRGBO(84, 89, 87, 100),
                    borderColor: Colors.white,
                    () {
                      setState(() {
                        _isFixEnable = !_isFixEnable;
                      });
                    },
                  ),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.micro),
                Container(
                  width: MediaQuery.sizeOf(context).width,
                  child: ComponentTextBtn(
                    '회원권 구매',
                    bgColor: Color.fromRGBO(130, 163, 129, 100),
                    borderColor: Colors.white,
                    () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              PageCustomerBuyTicket(
                                  customerId: widget.customerId)),
                    ),
                  ),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.micro),
                Container(
                  width: MediaQuery.sizeOf(context).width,
                  child: ComponentTextBtn(
                    '구매내용',
                    bgColor: Color.fromRGBO(84, 89, 87, 100),
                    borderColor: Colors.white,
                    () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              PageCustomerBuyHistory(customerId: widget.customerId)),
                    ),
                  ),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.micro),
                Container(
                  width: MediaQuery.sizeOf(context).width,
                  child: ComponentTextBtn(
                    '이용내역',
                    bgColor: Color.fromRGBO(84, 89, 87, 100),
                    borderColor: Colors.white,
                    () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              PageCustomerUseHistory()),
                    ),
                  ),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.micro),
                Container(
                  width: MediaQuery.sizeOf(context).width,
                  child: ComponentTextBtn(
                    '탈퇴',
                    bgColor: Color.fromRGBO(84, 89, 87, 100),
                    borderColor: Colors.white,
                    () {},
                  ),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.small),
              ],
            )),
      ),
    );
  }
}
