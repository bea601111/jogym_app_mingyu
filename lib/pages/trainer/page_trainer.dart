import 'package:flutter/material.dart';
import 'package:jo_gym_app/components/common/component_appbar_popup.dart';
import 'package:jo_gym_app/components/jogym_components/card/components_trainer_card.dart';
import 'package:jo_gym_app/config/config_style.dart';
import 'package:jo_gym_app/pages/trainer/page_trainer_create.dart';
import 'package:jo_gym_app/pages/trainer/page_trainer_detail.dart';

class PageTrainer extends StatefulWidget {
  const PageTrainer({super.key});

  @override
  State<PageTrainer> createState() => _PageTrainerState();
}

class _PageTrainerState extends State<PageTrainer> {
  final TextEditingController _searchTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "트레이너 정보 관리"),
      body: Column(
        children: [
          Container(
            padding: bodyPaddingLeftRightAndVerticalHalf,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    child: Flexible(
                  child: TextField(
                    controller: _searchTextController,
                    decoration: InputDecoration(
                      hintText: 'Search...',
                      // Add a clear button to the search bar
                      suffixIcon: IconButton(
                        icon: const Icon(Icons.clear),
                        onPressed: () => {_searchTextController.clear()},
                      ),
                      // Add a search icon or button to the search bar
                      prefixIcon: IconButton(
                        icon: const Icon(Icons.search),
                        onPressed: () {
                          // Perform the search here
                          print("Search :${_searchTextController.text}");
                        },
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                  ),
                )), // 회원검색
                const SizedBox(
                  width: 100,
                ),
                FloatingActionButton(
                  backgroundColor: Colors.blue,
                  tooltip: '신규 등록',
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                const PageTrainerCreate()));
                  },
                  child: Icon(
                    Icons.add,
                    size: 25,
                    color: Colors.white,
                  ),
                ), // 신규회원등록
              ],
            ), //신규 회원 등록 박스
          ),
          Expanded(
            child: SingleChildScrollView(
              padding: bodyPaddingLeftRight,
              child: Column(
                children: [
                  ComponentTrainerCard(
                    number: 1,
                    dateCreate: "2023-05-05",
                    trainerName: "로니콜먼",
                    phoneNumber: "010-0000-0001",
                    profileImg: Image.asset("assets/roni.png"),
                    voidCallback: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                              const PageTrainerDetail(trainerId: 1)));
                    },
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
