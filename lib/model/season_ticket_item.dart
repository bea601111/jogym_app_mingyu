class SeasonTicketItem {
  int id;
  int maxMonth;
  String ticketName;
  String ticketType;
  double totalPrice;
  double unitPrice;

  SeasonTicketItem(this.id, this.maxMonth, this.ticketName, this.ticketType, this.totalPrice, this.unitPrice);

  factory SeasonTicketItem.fromJson(Map<String, dynamic> json) {
    return SeasonTicketItem(
      json['id'],
      json['maxMonth'],
      json['ticketName'],
      json['ticketType'],
      json['totalPrice'],
      json['unitPrice'],
    );
  }
}
