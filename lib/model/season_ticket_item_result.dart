import 'package:jo_gym_app/model/season_ticket_item.dart';

class SeasonTicketItemResult {
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<SeasonTicketItem>? list;

  SeasonTicketItemResult(this.totalItemCount, this.totalPage, this.currentPage, {this.list});

  factory SeasonTicketItemResult.fromJson(Map<String, dynamic> json) {
    return SeasonTicketItemResult(
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      list: json['list'] != null ? (json['list'] as List).map((e) => SeasonTicketItem.fromJson(e)).toList() : [],
    );
  }
}