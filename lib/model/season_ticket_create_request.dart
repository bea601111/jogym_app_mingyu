class SeasonTicketCreateRequest {
  String ticketName;
  int maxMonth;
  double unitPrice;

  SeasonTicketCreateRequest(this.ticketName, this.maxMonth, this.unitPrice);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['ticketName'] = ticketName;
    data['maxMonth'] = maxMonth;
    data['unitPrice'] = unitPrice;

    return data;
  }
}