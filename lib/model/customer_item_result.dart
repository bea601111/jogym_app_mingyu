import 'package:jo_gym_app/model/customer_item.dart';

class CustomerItemResult {
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<CustomerItem>? list;

  CustomerItemResult(this.totalItemCount, this.totalPage, this.currentPage, {this.list});

  factory CustomerItemResult.fromJson(Map<String, dynamic> json) {
    return CustomerItemResult(
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      list: json['list'] != null ? (json['list'] as List).map((e) => CustomerItem.fromJson(e)).toList() : [],
    );
  }
}