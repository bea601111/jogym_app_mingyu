class PtTicketCreateRequest {
  String dateCreate;
  String storeMember;
  String trainerMember;
  String ticketName;
  String maxCount;
  String unitPrice;

  PtTicketCreateRequest(this.dateCreate, this.storeMember, this.trainerMember, this.ticketName, this.maxCount, this.unitPrice);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['dateCreate'] = dateCreate;
    data['storeMember'] = storeMember;
    data['trainerMember'] = trainerMember;
    data['ticketName'] = ticketName;
    data['maxCount'] = maxCount;
    data['unitPrice'] = unitPrice;

    return data;
  }
}