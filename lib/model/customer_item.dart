class CustomerItem {
  String dateCreate;
  int id;
  String name;
  String phoneNumber;

  CustomerItem(this.dateCreate, this.id, this.name, this.phoneNumber);

  factory CustomerItem.fromJson(Map<String, dynamic> json) {
    return CustomerItem(
      json['dateCreate'],
      json['id'],
      json['name'],
      json['phoneNumber'],
    );
  }
}
